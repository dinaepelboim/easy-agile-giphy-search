# Easy Agile Giphy Search assignment

## A Giphy search interface.

### Project development and production environment

## Demo: https://easy-agile-gyphy-search.herokuapp.com/

### Development mode:

##### git clone https://dinaepelboim@bitbucket.org/dinaepelboim/easy-agile-giphy-search.git

##### npm install

##### npm run dev

### Production mode:

##### git clone https://dinaepelboim@bitbucket.org/dinaepelboim/easy-agile-giphy-search.git

##### npm install

##### npm run build<

##### npm start<
