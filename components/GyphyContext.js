import { createContext } from "react";

const GhypyContext = createContext([[], () => {}]);

export default GhypyContext;
