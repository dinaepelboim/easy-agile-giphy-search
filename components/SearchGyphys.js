import React, { useContext, useEffect, useState } from "react";
import { MDBInput } from "mdbreact";
import { searchGyphysByName } from "../utils/apiCalls";
import GyphyContext from "../components/GyphyContext";

export const SearchGyphys = () => {
  const {
    offset,
    setOffset,
    limit,
    setLimit,
    setGyphys,
    setQuery,
    query
  } = useContext(GyphyContext);
  const [isFetching, setIsFetching] = useState(false);
  const [inputSearch, setInputSearch] = useState(false);

  const searchGyphys = async inputSearch => {
    const response = await searchGyphysByName(
      inputSearch,
      offset + limit,
      limit
    );
    if (response.error) return response;
    let { data } = response;
    setGyphys(data);
    setOffset(offset + limit);
    setLimit(offset + limit);
    setIsFetching(false);
    return data;
  };

  useEffect(() => {
    let inputSearch;
    function handleKeyUp(event) {
      event.preventDefault();
      inputSearch = event.target.value;
      if (inputSearch) {
        setQuery(inputSearch);
        setInputSearch(inputSearch);
        if (event.keyCode === 13 || inputSearch.length > 5) {
          event.preventDefault();
          searchGyphys(inputSearch);
        }
      }
    }
    document.addEventListener("keyup", handleKeyUp);
    return () => {
      document.removeEventListener("keyup", handleKeyUp);
    };
  }, []);
  useEffect(() => {
    function handleScroll() {
      if (
        window.innerHeight + document.documentElement.scrollTop !==
        document.documentElement.offsetHeight
      )
        return;
      searchGyphys(inputSearch);
      setIsFetching(true);
    }
    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  useEffect(() => {
    if (!isFetching) return;
    searchGyphys(query);
  }, [isFetching]);
  return (
    <>
      <MDBInput hint="Search" type="text" containerClass="mt-0" />
      <style jsx>{``}</style>
    </>
  );
};
