import React from "react";

const Fallback = () => {
  return (
    <h1 className="text-white text-center my-4">
      Something went wrong, please try again later.
    </h1>
  );
};

export default Fallback;
