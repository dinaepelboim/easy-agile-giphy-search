import React from "react";

const Tiles = ({ data }) => {
  debugger;
  return (
    <div>
      {data &&
        data.length > 0 &&
        data.map((item, index) => {
          return (
            <img
              key={item.id}
              src={item.images ? item.images.downsized_medium.url : ""}
              className="custom-search-img"
            ></img>
          );
        })}
      <style jsx>{`
        .custom-search-img {
          max-width: 200px;
          height: 180px;
          object-fit: cover;
          padding: 10px;
        }
      `}</style>
    </div>
  );
};

export default Tiles;
