import React, { useContext } from "react";
import GyphyContext from "../components/GyphyContext";
import { MDBBtn } from "mdbreact";
import { searchGyphysByName } from "../utils/apiCalls";

const SearchMore = () => {
  const { setGyphys, offset, limit, setLimit, setOffset, query } = useContext(
    GyphyContext
  );
  const handleClick = async () => {
    const moreData = await searchGyphys(query, offset + limit, limit);
    setGyphys(moreData);
    setOffset(offset + limit);
    setLimit(offset + limit);
  };
  const searchGyphys = async (query, offset, limit) => {
    const response = await searchGyphysByName(query, offset, limit);
    if (response.error) return response;
    let { data } = response;

    return data;
  };
  return <MDBBtn onClick={handleClick}>more</MDBBtn>;
};

export default SearchMore;
