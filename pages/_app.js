import React from "react";
import App from "next/app";
import "@fortawesome/fontawesome-free/css/all.min.css";
import "bootstrap-css-only/css/bootstrap.min.css";
import "mdbreact/dist/css/mdb.css";
import GyphyContext from "../components/GyphyContext";
import ErrorPage from "next/error";
import OfflineSupport from "../components/OfflineSupport";

export default class MyApp extends App {
  state = {
    gyphys: [],
    query: "",
    offset: 0,
    limit: 21
  };
  static async getInitialProps({ Component, ctx }) {
    let pageProps = {};
    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }
    return { pageProps };
  }

  handleSetGyphys = data => {
    this.setState(({ gyphys }) => ({
      gyphys: [data, ...gyphys]
    }));
  };

  setQuery = query => {
    this.setState({ query: query });
  };
  setOffset = offset => {
    this.setState({ offset: offset });
  };
  setLimit = limit => {
    this.setState({ limit: limit });
  };
  componentWillUnmount() {
    // this.setState({ gyphys: [] });
    this.setState({ query: "" });
  }

  render() {
    const { statusCode } = this.props;
    if (statusCode && statusCode !== 200) {
      return <ErrorPage statusCode={statusCode} />;
    }
    const { Component, pageProps } = this.props;
    return (
      <>
        <GyphyContext.Provider
          value={{
            gyphys: this.state.gyphys,
            setGyphys: this.handleSetGyphys,
            setQuery: this.setQuery,
            query: this.state.query,
            offset: this.state.offset,
            setOffset: this.setOffset,
            limit: this.state.limit,
            setLimit: this.setLimit
          }}
        >
          <OfflineSupport />
          <Component {...pageProps} />
        </GyphyContext.Provider>
      </>
    );
  }
}
