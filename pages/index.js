import React, { useContext } from "react";
import Head from "next/head";
import Fallback from "../components/Fallback";
import { getTrendingGyphys } from "../utils/apiCalls";
import Tiles from "../components/Tiles";
import { SearchGyphys } from "../components/SearchGyphys";
import { MDBContainer, MDBCol, MDBRow, MDBBtn } from "mdbreact";
import GyphyContext from "../components/GyphyContext";
import SearchMore from "../components/SearchMore";

const Home = ({ data, error }) => {
  const { gyphys, query } = useContext(GyphyContext);

  return (
    <>
      <Head>
        <title>Search Gyphys</title>
      </Head>
      {error ? (
        <Fallback />
      ) : (
        <>
          <MDBContainer fluid>
            <MDBRow className="d-flex  flex-row  justify-content-center">
              <MDBCol
                className="text-center my-3"
                autoFocus={true}
                role="search field"
              >
                <SearchGyphys className=""></SearchGyphys>
              </MDBCol>
            </MDBRow>
          </MDBContainer>
          <MDBContainer fluid>
            <MDBRow className="d-flex  flex-row  justify-content-center">
              <MDBCol className="text-center my-3">
                <Tiles
                  data={gyphys && gyphys.length > 0 && query ? gyphys[0] : data}
                />
              </MDBCol>
            </MDBRow>
            <MDBRow className="d-flex  flex-row  justify-content-center">
              <MDBCol className="text-center my-3">
                <SearchMore></SearchMore>
              </MDBCol>
            </MDBRow>
          </MDBContainer>
        </>
      )}
      <style jsx global>{`
        body {
          background-color: black;
        }
      `}</style>
    </>
  );
};

Home.getInitialProps = async () => {
  const response = await getTrendingGyphys();
  if (response.error) return response;
  let { data, error } = response;
  return { data, error };
};

export default Home;
