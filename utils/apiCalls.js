import "isomorphic-unfetch";
//import { API_KEY } from "./config";
const API_KEY = "BQs5PXvaPH71O4MBroW4UKVMfT40pn9k";

export const getTrendingGyphys = async () => {
  const url = `https://api.giphy.com/v1/gifs/trending?api_key=${API_KEY}&limit=21`;
  try {
    return await (await fetch(url)).json();
  } catch (err) {
    return { error: true };
  }
};
export const searchGyphysByName = async (query, offset, limit) => {
  const url = `https://api.giphy.com/v1/gifs/search?q=${query}&api_key=${API_KEY}&limit=${limit}&offset=${offset}`;
  try {
    return await (await fetch(url)).json();
  } catch (err) {
    return { error: true };
  }
};
